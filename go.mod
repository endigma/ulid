module gitlab.com/endigma/ulid

go 1.17

require (
	github.com/99designs/gqlgen v0.16.0
	github.com/oklog/ulid/v2 v2.0.2
)

require github.com/vektah/gqlparser/v2 v2.2.0 // indirect
