package ulid

import (
	"crypto/rand"
	"database/sql/driver"
	"fmt"
	"io"
	"strconv"
	"time"

	"github.com/99designs/gqlgen/graphql"
	"github.com/oklog/ulid/v2"
)

type ULID struct {
	ulid.ULID
}

var defaultEntropySource *ulid.MonotonicEntropy

func init() {
	// Seed the default entropy source.
	defaultEntropySource = ulid.Monotonic(rand.Reader, 0)
}

func New() ULID {
	return ULID{ulid.MustNew(ulid.Timestamp(time.Now()), defaultEntropySource)}
}

func (id ULID) Value() (driver.Value, error) {
	return id.String(), nil
}

func (id *ULID) Scan(src interface{}) error {
	return id.ULID.Scan(src)
}

func MarshalULID(u ULID) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		_, _ = io.WriteString(w, strconv.Quote(u.String()))
	})
}

func UnmarshalULID(v interface{}) (u ULID, err error) {
	s, ok := v.(string)
	if !ok {
		return u, fmt.Errorf("invalid type %T, expecting string", v)
	}
	ul, err := ulid.Parse(s)
	return ULID{ul}, err
}
